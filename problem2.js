/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs').promises
const { createFile, deleteJson, mainFunc } = require('./problem1')



const readData = async (fileName) => {
    let retData = await fs.readFile('./' + fileName, 'utf8')
    return retData
}

const writeData = async (data, split = undefined, join = undefined) => {
    let retFileName = await createFile(data, split, join, 'txt');
    return retFileName
}


const writeFileName = async (data) => {
    await fs.appendFile('./filenames.txt', data + "\n", function (err) {
        if (err) throw err;
    });
    return data
}

const deleteFiles = async () => {
    let files = await fs.readFile('filenames.txt', 'utf8')
    files = files.split('\n')
    for (let file_index = 0; file_index < files.length; file_index++) {
        
        let name = files[file_index];
        
        if (files[file_index].length > 0) await deleteJson(name);
    }
    await fs.truncate('filenames.txt', 0)
    console.log('Deleted Sucessfully')
}

const lipSumProb = async (count = 10) => {

    for (let index = 0; index < count; index++) {

        let retDataFileName;
        let retData = await readData('lipsum.txt')

        //1st part
        retData = retData.toUpperCase()
        retDataFileName = await writeData(retData)
        await writeFileName(retDataFileName)

        //2nd part
        retData = await readData(retDataFileName)
        retData = retData.toLowerCase()
        retDataFileName = await writeData(retData, ".", "\n", 'txt')
        await writeFileName(retDataFileName)

        //3rd part
        retData = await readData(retDataFileName)
        retData = retData.split('.').sort().join()
        retDataFileName = await writeData(retData)
        await writeFileName(retDataFileName)

        //deleting the files
        await deleteFiles()
    }
}

module.exports = lipSumProb
