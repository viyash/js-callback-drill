/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs')



const createFile = (cb, data = '', split = undefined, join = undefined, fileFormat = 'json') => {
    let name = Math.random()
    let fileName = `js-callback-${name}.${fileFormat}`;
    fs.writeFile(fileName, data.split(split).join(join), () => { cb(fileName) });
}

const readData = (fileName, cb) => {
    fs.readFile(fileName, 'utf8', (err, data) => { cb(data) })
}


const writeFileName = (fileName, cb) => {
    fs.appendFile('./filenames.txt', fileName + "\n", function (err, data) {
        if (err) throw err;
        cb(fileName)
    });
}


const deleteJson = (fileName) => {
    fs.access(fileName, (error) => {
        fs.unlink(fileName, (err) => {
        });
    });


}

const deleteFiles = () => {
    readData('filenames.txt', (data) => {
        let name = data.split('\n')
        for (let file_index = 0; file_index < name.length; file_index++) {
            let name_index = name[file_index];
            if (name_index.length > 0) deleteJson(name_index);
        }
        deleteJson('filenames.txt')
    })
}


const lipSumProb = (count = 1) => {
    for (let index = 0; index < count; index++) {

        readData('lipsum.txt', (data) => {
            createFile((data) => {
                writeFileName(data, (data) => {
                    readData(data, (element) => {
                        createFile((element2) => {
                            writeFileName(element2, (element3) => {
                                readData(element3, (element4) => {
                                    createFile((data) =>
                                        writeFileName(data, (data2) => {
                                            deleteFiles()
                                        }),
                                        element4.split(' ').sort().join(' '),
                                        split = undefined,
                                        join = undefined,
                                        'txt')
                                })
                            })
                        },
                            element.toLowerCase(),
                            ".",
                            "\n",
                            'txt')
                    })
                })
            }, data.toUpperCase(),
                split = undefined,
                join = undefined,
                'txt'
            )
        })
    }
}

module.exports = lipSumProb 
