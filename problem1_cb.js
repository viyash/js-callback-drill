const fs = require('fs')


const deleteJson = (fileName) => {
    fs.unlink('./js-dir/' + fileName, (err) => {
        if (err) {
            throw err;
        }
    });
}

const createFile = (cb, data = '', split = undefined, join = undefined, fileFormat = 'json') => {
    fs.mkdir("./js-dir/", () => {
        let name = Math.random()
        let fileName = `js-callback-${name}.${fileFormat}`;
        fs.writeFile('./js-dir/' + fileName, data.split(split).join(join), () => { cb(fileName) });
    })
}

const mainFunc = (cb, count = 1) => {
    for (let num = 0; num < count; num++) {
        createFile(cb)
    }
}


module.exports = { deleteJson, mainFunc }