const fs = require('fs').promises


const createFile = async (data = '', split = undefined, join = undefined, fileFormat = 'json') => {
    let name = Math.random()
    let fileName = `js-callback-${name}.${fileFormat}`;
    await fs.writeFile(fileName, data.split(split).join(join), (error) => {
        if (error) {
            console.log(data);
            throw error;
        }
    });
    console.log("File created successfully.");
    return fileName
}

const deleteJson = (fileName) => {
    return fs.unlink(__dirname +"/"+fileName, (err) => {
        if (err) {
            throw err;
        }
    });
}

const mainFunc = async (count = 10) => {
    let fileName;
    for (let num = 0; num < count; num++) {
        fileName = await createFile()
        await deleteJson(fileName)
    }

}

module.exports = { createFile, deleteJson, mainFunc }